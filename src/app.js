let theWheel = null;

const elBtn = document.getElementById("btn-spin");
const elUploadBtn = document.getElementById("names-upload");

elBtn.addEventListener("click", onSpinClicked);
elUploadBtn.addEventListener("change", onFileUploaded);


// Called by the onClick of the canvas, starts the spinning.
function onSpinClicked() {
  // Stop any current animation.
  theWheel.stopAnimation(false);

  // Reset the rotation angle to less than or equal to 360 so spinning again works as expected.
  // Setting to modulus (%) 360 keeps the current position.
  theWheel.rotationAngle = theWheel.rotationAngle % 360;

  // Start animation.
  theWheel.startAnimation();
}

function onFileUploaded() {
    
    const [ csv = null ] = elUploadBtn.files
    
    if (csv === null) {
        return;
    }
    
    const reader = new FileReader()
    reader.addEventListener('load', onFileRead)
    reader.readAsBinaryString(csv)

    
}

function onFileRead(event) {
    const { result = '' } = event.target

    if (!result) {
        throw new Error('Weird file!')
    }

    const segments = result.replace(/[\n']/g, '').toString().split(',').map(name => ({
        fillStyle: "#eae56f",
        text: name
    }))

    theWheel = initialiseWheel(segments);
    elBtn.disabled = false;
}

function initialiseWheel(segments) {
  return new Winwheel({
    numSegments: segments.length,
    textFontSize: 15,
    responsive: true, // This wheel is responsive!
    segments,
    pins: {
      outerRadius: 6,
      responsive: true, // This must be set to true if pin size is to be responsive.
    },
    animation: {
      type: "spinToStop",
      duration: 6,
      spins: 8,
    },
  });
}
