# Wheel of Fortune

Wheel of Fortune allows you to load some names and spin a wheel to randomly select someone.

## Installation

Use the package manager npm to install foobar.

```bash
npm install
```

## Usage

1. Add the names to names.txt (separate with , and next line)

```
Dean,
Timmy,
Charles
```

2. Start

```bash
npm run start
```
![Wheel of Fortune Running](../assets/wheel.png)

3. Choose files -> Names.txt

4. Click Spin
